@extends('layouts.main')

@section('content')
<div id="calculator">
	<div class="container">
		<div class="col-sm-10 col-offset-1 col-md-offset-1">
            <div class="panel panel-default" style="margin-top: 100px;">
                <div class="panel-heading">Конвертатор валют</div>
                <div class="panel-body">
					<form class="form-inline currencies">
						<label>Оберіть валюту</label>	
						<select class="form-control" id="fromFurrency">
						@foreach ($todayRates as $todayRate)
							<option value="{{ $todayRate->value }}">{{ $todayRate->code }} - {{ $todayRate->currency->description }}</option>
						@endforeach
						</select>	

						<label>Конвертувати в</label>	
						<select class="form-control" id="toCurrency">
						@foreach ($todayRates as $todayRate)
							<option value="{{ $todayRate->value }}">{{ $todayRate->code }} - {{ $todayRate->currency->description }}</option>
						@endforeach			
						</select>
					</form>

					<form class="form-inline amount">
						<label>Введіть суму</label>	
						<input type="text" class="form-control" value="0" id="value">
					</form>	
					<button type="button" id="calculate" class="btn btn-success">Порахувати</button>
				</div>
			</div>
			<label>Результат (значення у конвертованій валюті)</label>	
			<div id="result" class="form-control"></div>
		</div>
	</div>
</div>

@stop

@push('scripts')
	<script src="{{ asset('js/calculator.js') }}"></script> 	
@endpush