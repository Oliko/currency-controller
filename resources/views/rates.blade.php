@extends('layouts.main')

@section('content')
<div id="rates">
	<div class="container">
		<h2>Офіційний курс гривні щодо іноземних валют</h2>
		<form action="{{ route('rates') }}" method="GET" id="getCurrency" class="form-inline">
			<label>Оберіть дату, за якою вас цікавить курс</label>	
			<input type="date" name="date" value="{{ $date }}" class="form-control">
			<button type="submit" class="btn btn-success">Пошук</button>
		</form>


		<div class="col-sm-8 col-offset-2 col-md-offset-2">
			<table class="table table-striped" id="currencyTable">
			    <thead>
			      	<tr>
				        <th>Назва валюти</th>
				        <th>Офіційний курс (в гривнях)</th>
			      	</tr>
			    </thead>
			    <tbody>
			  	 	@foreach ($currencies as $currency)
			     	<tr>
				        <td>{{ $currency->code }} - {{ $currency->currency->description }}</td>
				        <td>{{ $currency->value }}</td>
			   		</tr>
			     	@endforeach
		      </tbody>
		    </table>
		</div>
	</div>	
</div>

@stop

@push('scripts')
	<script src="{{ asset('js/currency.js') }}"></script> 	
@endpush
