<!DOCTYPE html> 
<html lang="{{ config('app.locale') }}"> 
<head> 
	<meta charset="utf-8"> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<meta name="csrf-token" content="{{ csrf_token() }}"> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/styles.css') }}">


	<title>@yield('title') Валюта НБУ</title> 


</head> 

<body> 
	<header> 
		@include('partials.navbar')
	</header> 



	@yield('content') 


	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	
	@stack('scripts')
</body> 
</html>