@extends('layouts.main')

@section('content')
<div id="graph">
	<div class="container">
		<form action="{{ route('getGraph') }}" method="GET" id="getGraph">
	
	        <div class="panel panel-default" style="margin-top: 50px;">        
	            <div class="panel-body">
					<label>Оберіть валюту</label>	
					<select id="currency" name="currency" class="form-control">
					@foreach ($currencies as $currency)
						<option value="{{ $currency->code }}">{{ $currency->description }} ({{ $currency->code }})</option>
					@endforeach
					</select>

					<label>Оберіть дату, з якої дати вас цікавить курс</label>	
					<input type="date" name="dateFrom" id="dateFrom" value="{{ $date }}" class="form-control">
					<label>Оберіть дату, до якої дати вас цікавить курс</label>	
					<input type="date" name="dateTo" id="dateTo" value="{{ $date }}" class="form-control">

					<button type="submit" class="btn btn-success">Показати графік</button>
				</div>
			</div>
		</form>

	    <div id="linechart_material" style="width: 1000px; height: 600px"></div>
	</div>
</div>
@stop

@push('scripts')
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="{{ asset('js/graph.js') }}"></script> 	
@endpush