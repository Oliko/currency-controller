<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="{{ route('rates') }}">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="{{ route('rates') }}">Курси валют</a></li>
      <li><a href="{{ route('calculator') }}">Калькулятор валют</a></li>
      <li><a href="{{ route('graph') }}">Діаграма курсу</a></li>
    </ul>
  </div>
</nav>