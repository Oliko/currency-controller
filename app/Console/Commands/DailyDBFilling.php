<?php

namespace App\Console\Commands;

use App\Models\Currency;
use App\Models\ExchangeRate;
use DateTime;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Console\Command;

class DailyDBFilling extends Command
{
    protected $signature         = 'fill:currentDay';
    protected $description       = 'Command description';
    protected static $baseApiUrl = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange";

    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $date     = new DateTime('now');
        $url      = $this->generateApiUrl($date);
        $client   = new Guzzle();
        $response = $client->request('GET', $url);
        $rates    = json_decode($response->getBody()->getContents());

        foreach ($rates as $rate) {
            $currency = Currency::firstOrCreate(['code' => $rate->cc], ['description' => $rate->txt]);

            try {
                $exchangeRalue = ExchangeRate::create([
                    'code'  => $currency->code,
                    'date'  => $date->format('Y-m-d'),
                    'value' => $rate->rate,
                ]);
            } catch (\Illuminate\Database\QueryException $e) {
                echo "dublicate " . $currency->code . " " . $date->format('Y-m-d') . "\n";
            }
        }
    }

    public function generateApiUrl($date)
    {
        $params = [
            'json' => '',
            'date' => $date->format('Ymd'),
        ];

        return self::$baseApiUrl . '?' . http_build_query($params);
    }
}
