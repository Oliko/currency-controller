<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [  
        Commands\ParseExchange::class,
        Commands\DailyDBFilling::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('fill:currentDay')->dailyAt('00:30');
    }

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
