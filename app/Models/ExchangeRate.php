<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
	public $timestamps = false;
	
    public $fillable = [
    	'code',
    	'date',
    	'value'
    ];

    public function currency() 
    {
    	return $this->belongsTo('App\Models\Currency', 'code', 'code');
    }
}
