<?php

namespace App\Http\Controllers;

use App\Models\ExchangeRate;
use App\Models\Currency;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function rates(Request $request)
    {
    	$date = $request->date ?? date('Y-m-d');
    	$currencies = ExchangeRate::where('date', $date)->get();

    	return view('rates')
    		->with('currencies', $currencies)
    		->with('date', $date);
    }

    public function calculator()
    {
    	$date = date('Y-m-d');
    	$todayRates = ExchangeRate::where('date', $date)->get();

    	return view('calculator')
    		->with('todayRates', $todayRates);
    }

    public function graph()
    {
    	$date = date('Y-m-d');
    	$currencies = Currency::get()->sortBy('description');
    	return view('graph')
    		->with('currencies', $currencies)
    		->with('date', $date);
    }
}
