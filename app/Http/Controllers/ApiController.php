<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExchangeRate;

class ApiController extends Controller
{
	public function getGraph(Request $request)
	{
		$dateFrom = $request->dateFrom;
		$dateTo = $request->dateTo;
		$currency = $request->currency;

		$results = ExchangeRate::where('code', $currency)->whereBetween('date', [$dateFrom, $dateTo])->get();

		$response = [];
		foreach ($results as $result) {
			$response[] = [$result->date, $result->value ];
		}

		return response()->json($response);
	}
}
