<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeRatesTable extends Migration
{
    public function up()
    {
        Schema::create('exchange_rates', function (Blueprint $table) {
            $table->string('code');
            $table->foreign('code')->references('code')->on('currencies')->onDelete('cascade');
            $table->date('date');
            $table->primary(['code', 'date']);
            $table->double('value');
        });
    }

    public function down()
    {
        Schema::dropIfExists('exchange_rates');
    }
}
