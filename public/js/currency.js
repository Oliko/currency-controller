$("#getCurrency").submit(function(e) {
    let form = $(this);

    $.ajax({
        type: form.attr('type'),
        url: form.attr('action'),
        data: form.serialize(), 
        success: function(response) {
        	var table = $(response).find('#currencyTable').html();
        	$('#currencyTable').html(table);
        }
    });

	e.preventDefault(); 
});