$("#getGraph").submit(function(e) {
    let form = $(this);
    $.ajax({
        type: form.attr('type'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function(response) {
             google.charts.load('current', {
                'packages': ['line']
            });
            google.charts.setOnLoadCallback(function() {drawChart(response)});
        }
    });
    e.preventDefault();
});


function drawChart(response) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Дата');
    data.addColumn('number', 'Значення в грн');
    data.addRows(response);
    var options = {
        chart: {
            title: 'Даіграма змін валюти протягом вказаного часу'
        },
        width: 1000,
        height: 600
    };

    var chart = new google.charts.Line(document.getElementById('linechart_material'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}