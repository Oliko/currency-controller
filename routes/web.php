<?php

Route::get('/','PagesController@rates')->name('rates');
Route::get('/calculator','PagesController@calculator')->name('calculator');
Route::get('/graph','PagesController@graph')->name('graph');
Route::get('/getGraph','ApiController@getGraph')->name('getGraph');
